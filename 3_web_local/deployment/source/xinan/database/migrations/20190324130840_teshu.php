<?php

use think\migration\Migrator;
use think\migration\db\Column;

class Teshu extends Migrator
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('teshu');
        $table->addColumn('name','string')->addColumn('content','string')->addColumn('with','string')->addColumn('finish','integer',['default'=>0])->addColumn('time','string')->addColumn('agree','string',['default'=>'no'])->addColumn('end','string',['default'=>'no'])->create();
    }
}
