<?php
session_start();
require_once "config.php";
function hacker()
{
    show_error('f**k u, hacker!');
}

function random_hash(){
    $salt = 'hencehencehence';
    return substr(md5($salt.time()),0,8);
}

function Filter($string)
{
    global $mysqli;
    $blacklist = "information|benchmark|order|limit|join|file|into|execute|column|extractvalue|floor|update|insert|delete|username|password";
    $whitelist = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
    for ($i = 0; $i < strlen($string); $i++) {
        if (strpos("$whitelist", $string[$i]) === false) {
            hacker();
        }
    }
    if (preg_match("/$blacklist/is", $string)) {
        hacker();
    }
    if (is_string($string)) {
        return $mysqli->real_escape_string($string);
    } else {
        return "";
    }
}


function show_error($msg){
   die('<script>alert("' .$msg. '");window.location.href="/";</script>');
}

function xml_filter($content){
    $black_list = array('php://','gopher://');
    foreach($black_list as $black){
	//these protocols have been removed for security reason;
	if(strstr($content,$black)!==false){
	    show_error('what u have done to my xml?');
	}
    }	    
}

function read_info_from_docx($file_path){
    if(!is_file($file_path)){
	show_error('no such file!');	
    }
    $result = @file_get_contents("zip://".$file_path."#docProps/core.xml");
    xml_filter($result);
    libxml_disable_entity_loader(false);
    try{
	$xml = new SimpleXMLElement($result, LIBXML_NOENT);
	$xml->registerXPathNamespace("dc", "http://purl.org/dc/elements/1.1/"); 
	$xml->registerXPathNamespace("dcterms", "http://purl.org/dc/terms/"); 
	$meta = array();
	$meta['title'] = $xml->xpath('//dc:title')[0].''?$xml->xpath('//dc:title')[0].'':"empty";
	$meta['creator'] = $xml->xpath('//dc:creator')[0].''?$xml->xpath('//dc:creator')[0].'':"empty";
	$meta['subject'] = $xml->xpath('//dc:subject')[0].''?$xml->xpath('//dc:subject')[0].'':"empty";
	$meta['description'] = $xml->xpath('//dc:description')[0].''?$xml->xpath('//dc:description')[0].'':"empty";
	$meta['created'] = $xml->xpath('//dcterms:created')[0].''?$xml->xpath('//dcterms:created')[0].'':"empty";
	$meta['file_hash'] = substr($file_path,8,32);
	return $meta;
    }catch (Exception $e){
	show_error('error reading your docx file!');
    }
}
?>
